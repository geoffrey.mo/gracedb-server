# Generated by Django 3.2.9 on 2022-05-23 23:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('superevents', '0011_auto_20220314_0116'),
        ('events', '0056_add_lensing_labels'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='pipeline_preferred',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='pipeline_preferred_events', to='superevents.superevent'),
        ),
    ]
