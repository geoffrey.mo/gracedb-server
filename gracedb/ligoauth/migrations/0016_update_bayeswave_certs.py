# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-11 18:27
from __future__ import unicode_literals

from django.db import migrations

ACCOUNT_NAME = 'bayeswave'
NEW_CERT = '/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=ldas-grid.ligo.caltech.edu/CN=bayeswave_online/CN=Bence Becsy/CN=UID:bence.becsy.robot'
OLD_CERTS = [
    '/DC=org/DC=ligo/O=LIGO/OU=Services/CN=bw_online/ldas-grid.ligo.caltech.edu',
    '/DC=org/DC=ligo/O=LIGO/OU=Services/CN=bayeswave/ldas-grid.ligo.caltech.edu',
]


def update_certs(apps, schema_editor):
    User = apps.get_model('auth', 'User')

    # Get user
    user = User.objects.get(username=ACCOUNT_NAME)

    # Create new certificate
    user.x509cert_set.create(subject=NEW_CERT)

    # Delete old certificates
    for subj in OLD_CERTS:
        old_cert = user.x509cert_set.get(subject=subj)
        old_cert.delete()


def revert_certs(apps, schema_editor):
    User = apps.get_model('auth', 'User')
    X509Cert = apps.get_model('ligoauth', 'X509Cert')

    # Delete new certificate
    new_cert = X509Cert.objects.get(subject=NEW_CERT)
    new_cert.delete()

    # Get user
    user = User.objects.get(username=ACCOUNT_NAME)

    # Create old certificates
    for subj in OLD_CERTS:
        user.x509cert_set.create(subject=subj)


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0015_delete_hinj_cert'),
    ]

    operations = [
        migrations.RunPython(update_certs, revert_certs),
    ]
