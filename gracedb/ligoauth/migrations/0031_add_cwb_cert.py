# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2019-05-14 17:48
from __future__ import unicode_literals

from django.db import migrations

ACCOUNT = {
    'name': 'waveburst',
    'new_cert': '/DC=org/DC=cilogon/C=US/O=LIGO/OU=Robots/CN=ligo.caltech.edu/CN=cwbonline/CN=Marco Drago/CN=UID:marco.drago.robot',
}


def add_cert(apps, schema_editor):
    RobotUser = apps.get_model('ligoauth', 'RobotUser')

    # Get user
    user = RobotUser.objects.get(username=ACCOUNT['name'])

    # Create new certificate
    user.x509cert_set.create(subject=ACCOUNT['new_cert'])


def delete_cert(apps, schema_editor):
    RobotUser = apps.get_model('ligoauth', 'RobotUser')

    # Get user
    user = RobotUser.objects.get(username=ACCOUNT['name'])

    # Delete new certificate
    cert = user.x509cert_set.get(subject=ACCOUNT['new_cert'])
    cert.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ligoauth', '0030_update_spiir_acct_and_certs'),
    ]

    operations = [
        migrations.RunPython(add_cert, delete_cert),
    ]
